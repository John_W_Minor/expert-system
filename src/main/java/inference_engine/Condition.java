package inference_engine;

class Condition
{
    final Fact condition;
    final boolean positive;

    Condition(Fact _condition, boolean _positive)
    {
        condition = _condition;
        positive = _positive;
    }

    @Override
    public String toString()
    {
        if(positive)
        {
            return condition.name;
        }

        return "!" + condition.name;
    }

    @Override
    public boolean equals(Object _obj)
    {
        if(this == _obj)
        {
            return true;
        }

        if(!(_obj instanceof Condition))
        {
            return false;
        }

        Condition otherCondition = (Condition) _obj;

        return toString().equals(otherCondition.toString());
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
}
