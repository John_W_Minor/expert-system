package inference_engine;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class RuleMap
{
    private final Map<String, RuleBucket> rulesByName = new HashMap<>();
    private final Set<Fact> allFacts = new HashSet<>();

    void addRule(String _label, Rule _rule)
    {
        allFacts.addAll(_rule.getConditionFacts());
        allFacts.add(_rule.conclusion);

        {
            RuleBucket bucket = rulesByName.get(_label);

            if (bucket == null)
            {
                bucket = new RuleBucket();
            }

            bucket.addRule(_rule);

            rulesByName.put(_label, bucket);
        }
    }

    void removeRule(String _label, Rule _rule)
    {
        RuleBucket bucket = rulesByName.get(_label);

        if (bucket == null)
        {
            return;
        }

        bucket.removeRule(_rule);

        rulesByName.put(_label, bucket);
    }

    void removeRulesByLabel(String _label)
    {
        rulesByName.remove(_label);
    }

    Set<Rule> getRules()
    {
        Set<Rule> rules = new HashSet<>();

        for(Entry<String, RuleBucket> entry : rulesByName.entrySet())
        {
            RuleBucket bucket = entry.getValue();

            rules.addAll(bucket.getRules());
        }

        return rules;
    }

    Set<Rule> getRulesByConclusion(Fact _conclusion)
    {
        Set<Rule> rules = new HashSet<>();

        for(Entry<String, RuleBucket> entry : rulesByName.entrySet())
        {
            RuleBucket bucket = entry.getValue();

            Set<Rule> bucketRules = bucket.getRules();

            for(Rule bucketRule : bucketRules)
            {
                if(bucketRule.conclusion.equals(_conclusion))
                {
                    rules.add(bucketRule);
                }
            }
        }

        return rules;
    }

    Set<Fact> getAllFacts()
    {
        return new HashSet<>(allFacts);
    }
}
