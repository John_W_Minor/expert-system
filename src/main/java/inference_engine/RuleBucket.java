package inference_engine;

import java.util.HashSet;
import java.util.Set;

class RuleBucket
{
    private final Set<Rule> rules = new HashSet<>();

    void addRule(Rule _rule)
    {
        rules.add(_rule);
    }

    void removeRule(Rule _rule)
    {
        rules.remove(_rule);
    }

    Set<Rule> getRules()
    {
        return new HashSet<>(rules);
    }

    boolean contains(Rule _rule)
    {
        return rules.contains(_rule);
    }
}
