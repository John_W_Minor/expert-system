package inference_engine;

class Fact
{
    final String name;

    Fact(String _name)
    {
        name = _name.trim().toLowerCase();
    }

    @Override
    public String toString()
    {
        return name;
    }

    @Override
    public boolean equals(Object _obj)
    {
        if(this == _obj)
        {
            return true;
        }

        if(!(_obj instanceof Fact))
        {
            return false;
        }

        Fact otherFact = (Fact) _obj;

        return name.equals(otherFact.name);
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }
}
