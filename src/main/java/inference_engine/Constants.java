package inference_engine;

class Constants
{
    static final String TAB = "    ";
    static final String UNKNOWN_INPUT = TAB + "unknown input\n";
}
