package inference_engine;

import java.util.*;

class RuleTreeNode
{
    private final Rule rule;

    private final Set<RuleTreeNode> branches = new HashSet<>();

    private boolean satisfied;

    RuleTreeNode(Rule _rule)
    {
        rule = _rule;
    }

    void satisfy()
    {
        satisfied = true;
    }

    void addNode(RuleTreeNode _node)
    {
        branches.add(_node);
    }

    //region On success methods for analyzing logic.
    void prune()
    {
        Iterator<RuleTreeNode> iter = branches.iterator();

        while (iter.hasNext())
        {
            RuleTreeNode node = iter.next();
            if (!node.satisfied)
            {
                iter.remove();
                continue;
            }
            node.prune();
        }
    }

    Set<Condition> getNegatives()
    {
        Set<Condition> negatives = new HashSet<>();

        if(rule != null)
        {
            for(Condition condition : rule.getConditions())
            {
                if(!condition.positive)
                {
                    negatives.add(condition);
                }
            }
        }

        for (RuleTreeNode node : branches)
        {
            negatives.addAll(node.getNegatives());
        }

        return negatives;
    }
    //endregion

    //region On failure methods for analyzing logic.
    Set<Rule> makeSuggestions(Fact _condition, Fact _conclusion)
    {
        Set<RuleTreeNode> terminals = findTerminalNodes();

        Set<Rule> suggestions = new HashSet<>();

        for (RuleTreeNode terminal : terminals)
        {
            Rule rule = terminal.rule;

            {
                Fact conclusion = rule.conclusion;

                suggestions.add(new Rule(_condition, conclusion));
            }

            for(Condition suggestionCondition : rule.getConditions())
            {
                Fact conclusion = suggestionCondition.condition;

                suggestions.add(new Rule( _condition , conclusion));
            }
        }

        Rule selfRule = new Rule(_condition, _condition);
        suggestions.remove(selfRule);

        Rule trivialRule = new Rule(_condition, _conclusion);
        suggestions.add(trivialRule);

        return suggestions;
    }

    private Set<RuleTreeNode> findTerminalNodes()
    {
        Set<RuleTreeNode> terminals = new HashSet<>();

        //Final all trivial terminal nodes.
        for (RuleTreeNode node : branches)
        {
            if (node.branches.size() == 0)
            {
                terminals.add(node);
            } else
            {
                terminals.addAll(node.findTerminalNodes());
            }
        }

        //Some nodes may have conditions on their rules that couldn't be tested, which makes them also terminal nodes.
        if (rule != null)
        {
            for (Fact maybeTestedCondition : rule.getConditionFacts())
            {
                boolean tested = false;

                for (RuleTreeNode node : branches)
                {
                    if (maybeTestedCondition.equals(node.rule.conclusion))
                    {
                        tested = true;
                        break;
                    }
                }

                if (!tested)
                {
                    terminals.add(this);
                    break;
                }
            }
        }

        return terminals;
    }
    //endregion

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        int indentation = 0;

        if (rule != null)
        {
            builder.append(rule).append("\n");
            indentation = 1;
        }

        for (RuleTreeNode node : branches)
        {
            builder.append(node.toString(indentation));
        }

        return builder.toString();
    }

    public String toString(int _indentation)
    {
        StringBuilder builder = new StringBuilder();

        if (rule != null)
        {
            for (int i = 0; i < _indentation; i++)
            {
                builder.append(Constants.TAB);
            }
            builder.append(rule).append("\n");
        }

        for (RuleTreeNode node : branches)
        {
            builder.append(node.toString(_indentation + 1));
        }

        return builder.toString();
    }

    @Override
    public boolean equals(Object _obj)
    {
        if (this == _obj)
        {
            return true;
        }

        if (!(_obj instanceof RuleTreeNode))
        {
            return false;
        }

        RuleTreeNode otherFact = (RuleTreeNode) _obj;

        return rule.equals(otherFact.rule);
    }

    @Override
    public int hashCode()
    {
        return rule.hashCode();
    }
}
