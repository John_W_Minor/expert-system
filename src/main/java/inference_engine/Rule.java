package inference_engine;


import java.util.*;

class Rule
{
    private final List<Condition> conditions;
    private final List<Fact> conditionFacts;
    final Fact conclusion;

    Rule(Fact _condition, Fact _conclusion)
    {
        conditions = new ArrayList<>();
        Condition condition = new Condition(_condition, true);
        conditions.add(condition);

        conditionFacts = new ArrayList<>();
        conditionFacts.add(_condition);

        conclusion = _conclusion;
    }

    Rule(Collection<Condition> _conditions, Fact _conclusion)
    {
        conditions = new ArrayList<>(_conditions);

        conditions.sort(Comparator.comparing(Condition::toString).reversed());

        conditionFacts = new ArrayList<>();

        for(Condition condition : conditions)
        {
            conditionFacts.add(condition.condition);
        }

        conclusion = _conclusion;
    }

    List<Condition> getConditions()
    {
        return new ArrayList<>(conditions);
    }

    List<Fact> getConditionFacts()
    {
        return new ArrayList<>(conditionFacts);
    }

    List<Rule> getBidirectionalRules()
    {
        List<Rule> bidirectionalRules = new ArrayList<>();

        Fact newCondition = conclusion;

        for(Condition newConclusion : conditions)
        {
            if(newConclusion.positive)
            {
                Rule bidirectionalRule = new Rule(newCondition, newConclusion.condition);
                bidirectionalRules.add(bidirectionalRule);
            }
        }

        bidirectionalRules.add(this);

        return bidirectionalRules;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        Iterator<Condition> iter = conditions.iterator();

        while(iter.hasNext())
        {
            builder.append(iter.next());

            if(iter.hasNext())
            {
                builder.append(" & ");
            }
        }

        return builder + " > " + conclusion;
    }

    @Override
    public boolean equals(Object _obj)
    {
        if(this == _obj)
        {
            return true;
        }

        if(!(_obj instanceof Rule))
        {
            return false;
        }

        Rule otherRule = (Rule) _obj;

        return toString().equals(otherRule.toString());
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
}
