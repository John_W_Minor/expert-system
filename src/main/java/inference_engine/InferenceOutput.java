package inference_engine;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class InferenceOutput
{
    public final boolean success;
    public final Rule inferredRule;
    private final RuleTreeNode root;
    public final boolean deduction;

    private String toStringOutput = null;

    private Set<Rule> suggestions = new HashSet<>();

    InferenceOutput(boolean _success, Fact _condition, Fact _conclusion, RuleTreeNode _root, InferenceEngine _engine)
    {
        success = _success;
        root = _root;

        if (success)
        {

            root.prune();

            Set<Condition> conditions = root.getNegatives();

            deduction = conditions.size() == 0;

            for(Condition negative : conditions)
            {
                Fact conclusion = negative.condition;

                suggestions.add(new Rule(_condition, conclusion));
            }

            Condition givenCondition = new Condition(_condition, true);
            conditions.add(givenCondition);

            inferredRule = new Rule(conditions, _conclusion);

        } else
        {

            deduction = false;

            inferredRule = new Rule(_condition, _conclusion);

            //Get suggestions.
            suggestions = root.makeSuggestions(_condition, _conclusion);

            //region Remove suggestions known to be true.
            Iterator<Rule> iter = suggestions.iterator();

            while (iter.hasNext())
            {
                Rule rule = iter.next();

                Fact suggestedConclusion = rule.conclusion;

                ConclusionTracker tempTracker = new ConclusionTracker(suggestedConclusion);
                RuleTreeNode tempRoot = new RuleTreeNode(null);

                if (_engine.infer(_condition, suggestedConclusion, tempTracker, tempRoot))
                {
                    iter.remove();
                }
            }
            //endregion

        }
    }

    public Set<Rule> getSuggestions()
    {
        return new HashSet<>(suggestions);
    }

    @Override
    public String toString()
    {
        return toString(0);
    }

    public String toString(int _indentation)
    {
        StringBuilder builder = new StringBuilder();

        StringBuilder tabs = new StringBuilder();

        for (int i = 0; i < _indentation; i++)
        {
            tabs.append(Constants.TAB);
        }

        builder.append(tabs);

        if (success)
        {
            builder.append("(").append(inferredRule).append(") ");

            if(deduction)
            {
                builder.append("given by deduction:\n");
            } else
            {
                builder.append("given by induction:\n");
            }

            builder.append(root.toString(_indentation));

            return builder.toString();
        } else
        {
            builder.append("(").append(inferredRule).append(") not implied by given rules\n\n");

            if (suggestions.size() != 0)
            {
                builder.append(tabs);

                builder.append("affirming any of these hypothetical rules may allow further analysis:\n\n");

                for (Rule suggestion : suggestions)
                {
                    builder.append(tabs).append(Constants.TAB).append(suggestion).append("\n");
                }
            }

            return builder.toString();
        }
    }
}
