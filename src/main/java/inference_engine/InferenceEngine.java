package inference_engine;

import java.util.*;

public class InferenceEngine
{
    private RuleMap ruleMap = new RuleMap();

    private List<String> lastQuestion = null;

    private List<String> tokenizer(String _input)
    {
        List<String> tokens = new ArrayList<>();

        _input = _input.toLowerCase();

        StringBuilder tokenBuilder = new StringBuilder();

        while (true)
        {
            String character = _input.substring(0, 1);
            _input = _input.substring(1);

            if (character.matches("\\s|[a-z]"))
            {
                tokenBuilder.append(character);
            } else if (character.equals("<") && _input.length() != 0 && _input.substring(0, 1).equals(">"))
            {
                String token = tokenBuilder.toString().trim();
                tokenBuilder = new StringBuilder();
                if (!token.equals(""))
                {
                    tokens.add(token);
                }

                _input = _input.substring(1);

                tokens.add("<>");
            } else
            {
                String token = tokenBuilder.toString().trim();
                tokenBuilder = new StringBuilder();
                if (!token.equals(""))
                {
                    tokens.add(token);
                }

                tokens.add(character);
            }

            if (_input.length() == 0)
            {
                String token = tokenBuilder.toString().trim();
                if (!token.equals(""))
                {
                    tokens.add(token);
                }

                break;
            }
        }

        return tokens;
    }

    public String input(String _input)
    {
        return input(tokenizer(_input));
    }

    private String input(List<String> _tokens)
    {
        boolean declaration = _tokens.size() >= 5 && _tokens.get(1).equals(":");
        boolean question = _tokens.size() == 3 && _tokens.get(1).equals("?");
        boolean deleteRuleLabel = _tokens.size() == 3 && _tokens.get(1).equals(":") && _tokens.get(2).equals("/");
        boolean deleteRuleLabelQuestion = _tokens.size() == 4 && _tokens.get(1).equals(":") && _tokens.get(2).equals("/") && _tokens.get(3).equals("?");

        //region declaration
        if (declaration)
        {
            String label = _tokens.get(0);

            if(!label.matches("(\\s|[a-z])+"))
            {
                return Constants.UNKNOWN_INPUT;
            }

            List<Condition> conditions = new ArrayList<>();
            Fact conclusion = null;


            Iterator<String> iter = _tokens.iterator();

            iter.next();
            iter.remove();

            iter.next();
            iter.remove();

            boolean positive = true;
            boolean gatheringConditions = true;
            boolean forward = false;
            boolean bidirection = false;
            boolean delete = false;
            boolean repeatLastQuestion = false;

            while (iter.hasNext())
            {
                String token = iter.next();

                if(token.equals("!") && gatheringConditions)
                {
                    positive = false;
                } else if(token.matches("(\\s|[a-z])+") && gatheringConditions)
                {
                    Fact fact = new Fact(token);
                    Condition condition = new Condition(fact, positive);
                    positive = true;

                    conditions.add(condition);
                } else if(!token.equals("&") && gatheringConditions)
                {
                    forward = token.equals(">");
                    bidirection = token.equals("<>");
                    delete = token.equals("/");

                    gatheringConditions = false;
                } else if(token.matches("(\\s|[a-z])+") && !gatheringConditions)
                {
                    if(conclusion != null)
                    {
                        return Constants.UNKNOWN_INPUT;
                    }

                    conclusion = new Fact(token);
                } else if(token.equals("?") && !gatheringConditions)
                {
                    if (conclusion == null)
                    {
                        return Constants.UNKNOWN_INPUT;
                    }

                    repeatLastQuestion = true;
                }
            }

            if(!forward && !bidirection && !delete)
            {
                return Constants.UNKNOWN_INPUT;
            }

            if(conditions.size() == 0 || conclusion == null)
            {
                return Constants.UNKNOWN_INPUT;
            }

            Condition positiveTest = new Condition(conclusion, true);
            Condition negativeTest = new Condition(conclusion, false);

            if(conditions.contains(positiveTest) || conditions.contains(negativeTest))
            {
                return Constants.TAB + "illegal self-reference in rule\n";
            }

            Rule rule = new Rule(conditions, conclusion);

            StringBuilder builder = new StringBuilder();

            if(forward)
            {
                addRule(label, rule);

                builder.append(Constants.TAB + "added rule\n");
            }

            if(bidirection)
            {
                List<Rule> bidirectionalRules = rule.getBidirectionalRules();

                for(Rule bidirectionalRule : bidirectionalRules)
                {
                    addRule(label, bidirectionalRule);
                }

                builder.append(Constants.TAB + "added bidirectional rule\n");
            }

            if(delete)
            {
                removeRule(label, rule);

                builder.append(Constants.TAB + "removed rule\n");
            }

            if(repeatLastQuestion)
            {
                if(lastQuestion != null)
                {
                    builder.append("\n").append(input(lastQuestion));
                } else
                {
                    builder.append("\n" + Constants.TAB + "no question available in memory for re-evaluation\n");
                }
            }

            return builder.toString();
        }
        //endregion

        //region question
        if (question)
        {
            lastQuestion = new ArrayList<>(_tokens);

            String conditionString = _tokens.get(0);
            String conclusionString = _tokens.get(2);

            Fact conclusion = new Fact(conclusionString);

            if(conditionString.equals("*"))
            {
                StringBuilder builder = new StringBuilder();

                List<InferenceOutput> inferenceOutputs = new ArrayList<>();

                for (Fact condition : ruleMap.getAllFacts())
                {
                    InferenceOutput inferenceOutput = infer(condition, conclusion);

                    if (inferenceOutput.success)
                    {
                        inferenceOutputs.add(inferenceOutput);
                    }
                }

                Iterator<InferenceOutput> iter = inferenceOutputs.iterator();

                while(iter.hasNext())
                {
                    InferenceOutput inferenceOutput = iter.next();

                    builder.append(inferenceOutput.toString(2));

                    if(iter.hasNext())
                    {
                        builder.append("\n\n");
                    }
                }

                String out = builder.toString();

                if (out.equals(""))
                {
                    out = Constants.TAB + Constants.TAB + "no matches found\n";
                }

                return Constants.TAB + "matches:\n\n" + out;

            } else
            {
                Fact condition = new Fact(conditionString);

                return Constants.TAB + "conclusion:\n\n" + infer(condition, conclusion).toString(2);
            }
        }
        //endregion

        //region deleteRuleLabel
        if(deleteRuleLabel)
        {
            String label = _tokens.get(0);

            if(!label.matches("(\\s|[a-z])+"))
            {
                return Constants.UNKNOWN_INPUT;
            }

            removeRulesByLabel(label);

            return Constants.TAB + "removed rules\n";
        }
        //endregion

        //region deleteRuleLabelQuestion
        if(deleteRuleLabelQuestion)
        {
            String label = _tokens.get(0);

            if(!label.matches("(\\s|[a-z])+"))
            {
                return Constants.UNKNOWN_INPUT;
            }

            removeRulesByLabel(label);

            StringBuilder builder = new StringBuilder();

            builder.append(Constants.TAB + "removed rules\n").append("\n");

            if(lastQuestion != null)
            {
                builder.append(input(lastQuestion));
            } else
            {
                builder.append(Constants.TAB + "no question available in memory for re-evaluation\n");
            }

            return builder.toString();
        }
        //endregion

        return Constants.UNKNOWN_INPUT;
    }

    public void addRule(String _label, Rule _rule)
    {
        //Prevent illegal self-reference, which can pollute suggestions.
        Fact conclusion = _rule.conclusion;

        Condition positiveTest = new Condition(conclusion, true);
        Condition negativeTest = new Condition(conclusion, false);

        List<Condition> conditions = _rule.getConditions();

        if(conditions.contains(positiveTest) || conditions.contains(negativeTest))
        {
            return;
        }

        ruleMap.addRule(_label, _rule);
    }

    public void removeRule(String _label, Rule _rule)
    {
        ruleMap.removeRule(_label, _rule);
    }

    public void removeRulesByLabel(String _label)
    {
        ruleMap.removeRulesByLabel(_label);
    }

    public InferenceOutput infer(Fact _condition, Fact _conclusion)
    {
        ConclusionTracker tracker = new ConclusionTracker(_conclusion);
        RuleTreeNode root = new RuleTreeNode(null);

        boolean success = infer(_condition, _conclusion, tracker, root);

        return new InferenceOutput(success, _condition, _conclusion, root, this);
    }

    Boolean infer(Fact _condition, Fact _conclusion, ConclusionTracker _tracker, RuleTreeNode _node)
    {
        //Test the trivial case.
        if (_condition.equals(_conclusion))
        {
            Rule rule = new Rule(_condition, _conclusion);
            RuleTreeNode newNode = new RuleTreeNode(rule);
            _node.addNode(newNode);

            newNode.satisfy();
            return true;
        }

        //We grab the rules that are relevant to what we're doing.
        Set<Rule> rules = ruleMap.getRulesByConclusion(_conclusion);

        //Look through all the rules.
        for (Rule rule : rules)
        {
            //We'll set up the new node in the rule tree for this rule.
            RuleTreeNode newNode = new RuleTreeNode(rule);
            _node.addNode(newNode);

            //Now we need the rule's conditions to somehow come back true.
            boolean result = true;

            List<Condition> conditions = rule.getConditions();

            for (Condition currentCondition : conditions)
            {
                Fact newConclusion = currentCondition.condition;
                boolean positive = currentCondition.positive;

                //If we've already encountered this conclusion, positive or negative, on the current branch of the search we'll skip it to avoid lewps.
                //This also means we've found a dead-end in our search, tho we'll keep looking on the other branches of the node in case they provide more insight into the problem.
                if (_tracker.contains(newConclusion))
                {
                    result = false;
                    continue;
                }

                //Start tracking the conclusion.
                _tracker.push(newConclusion);

                boolean hypothesis = positive == infer(_condition, newConclusion, _tracker, newNode);

                if (!hypothesis)
                {
                    result = false;
                }

                //We're done checking the conclusion, so we'll stop tracking it.
                _tracker.pop();
            }

            if (result)
            {
                //If the new node came back true its conditions are satisfied.
                //Nodes that are not satisfied will get pruned later, as with their child branches.
                newNode.satisfy();
                return true;
            }
        }

        //If no good rule was found, then the method will fall through to false.
        return false;
    }
}
