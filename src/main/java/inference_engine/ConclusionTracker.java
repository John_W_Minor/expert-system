package inference_engine;

import java.util.Stack;

class ConclusionTracker
{
    private final Stack<Fact> used = new Stack<>();

    ConclusionTracker(Fact _rootConclusion)
    {
        used.push(_rootConclusion);
    }

    void push(Fact _newConclusion)
    {
        used.push(_newConclusion);
    }

    void pop()
    {
        used.pop();
    }

    boolean contains(Fact _newConclusion)
    {
        return used.contains(_newConclusion);
    }
}
