package expert_testbed;

import inference_engine.*;

public class Main
{
    private static InferenceEngine engine = new InferenceEngine();
    public static void main(String[] _args)
    {
        ///*
        input("cheetah: feline & spots & fast <> cheetah");
        input("feline: mammal & carnivore & whiskers & !dog <> feline");
        input("mammal: milk & fur & !reptile & !avian & !fish <> mammal");
        input("carnivore: eats meat & !eats plants <> carnivore");
        input("leah: leah > eats meat");
        input("leah: leah > spots");
        input("leah: leah > fast");
        input("leah: leah > whiskers");
        input("leah ? cheetah");

        input("leah: leah > milk ?");
        input("leah: leah > fur ?");
        input("leah: leah > reptile ?");
        //*/

        /*
        input("leah: leah > milk");

        input("leah: leah > fur");

        input("leah: leah > eats meat");

        input("leah: leah > spots");

        input("leah: leah > fast");

        input("leah: leah > whiskers");


        input("mammal: milk & fur & !reptile & !avian & !fish <> mammal");


        input("carnivore: eats meat & !eats plants <> carnivore");


        input("feline: mammal & carnivore & whiskers & !dog <> feline");


        input("cheetah: feline & spots & fast <> cheetah");


        input("maximo: maximo > cheetah");


        input("tigerlilly: tigerlilly > feline");

        input("tigerlilly: tigerlilly > spots");

        input("tigerlilly: tigerlilly > fast");


        input("leah ? cheetah");

        //input("print rules");

        //input("* ? cheetah");
        //*/


        /*
        while (true)
        {
            System.out.println(engine.input(System.console().readLine()));
        }
        //*/
    }

    private static void input(String _input)
    {
        System.out.println(_input);
        System.out.println(engine.input(_input));
    }
}
